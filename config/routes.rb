Rails.application.routes.draw do
  root to: 'products#index'
  get 'products/:product_id/donate', to: 'charges#prepare'
  post 'products/:product_id/charges/new', to: 'charges#new'


  devise_for :users, controllers: {omniauth_callbacks: 'omniauth_callbacks'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :products do
    resources :charges
  end


  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

end
