# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

  Product.create!(description: "A sectional sofa is modular. It usually includes at least two pieces that can be arranged in multiple configurations. A sectional sofa often has the option to choose the arm position as either left or right, with two or no arms. The key benefit of a sectional sofa is the ability to tailor its components to a range of spaces. Although sectional sofas were manufactured as early as the Victorian era, they did not become prominent until the mid-1950s, and they continue to be refined to the present day. At its most extreme, the Lava Sofa by Studio Vertijet -- a soft, flowing sectional -- can be arranged in any number of ways including crossed, paralleled or stacked."
                  email: email,
                  password:              password,
                  password_confirmation: password,
                  activated: true,
                  activated_at: Time.zone.now)
end
