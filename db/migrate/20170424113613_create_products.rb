class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.references :user
      t.string :title
      t.text :description
      t.float :target
      t.datetime  :end_time
      t.timestamps
    end
  end
end
