require 'rails_helper'

  RSpec.describe "AddNewFund", type: :feature do

    scenario "emails all users when new fund added" do
      user = create(:user)
      visit root_path
      click_on 'Log in'
      expect(current_path).to eq(new_user_session_path)
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      click_button 'Log in'
      expect(current_path).to eq(root_path)
      expect(page).to have_content('Signed in successfully')
      expect(AlertWorker.jobs.size).to eq(0)
      click_on 'Start a campaign'
      expect(current_path).to eq(new_product_path)
      fill_in 'product[title]', with: 'fund'
      fill_in 'product[description]', with: 'Test fund'
      fill_in 'product[target]', with: 3000
      click_button 'Submit'
      expect(AlertWorker.jobs.size).to eq(1)
      expect(current_path).to eq(products_path)
    end
  end
