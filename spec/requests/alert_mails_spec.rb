require 'rails_helper'

RSpec.describe "Test sending email with sidekiq", type: :request do
  it 'send email to sidekiq' do
    expect{ AlertWorker.perform_async(create(:product))}.to change(AlertWorker.jobs, :size).by(1)
  end
end
