FactoryGirl.define do
  factory :user do
    sequence(:username)  { |n| "Yaoie#{n}" }
    sequence(:email)     { |n| "#{username}@example.com"}
    password  'password'
    admin     true
  end

  factory :product do
    title      'fund'
    description    'a test fund'
    target      2000
    user
  end
end
