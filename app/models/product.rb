class Product < ApplicationRecord

  include PgSearch

  before_validation :set_end_time
  pg_search_scope :search, against: [:title, :description],
                  using: {tsearch: {dictionary: 'english', prefix: true, any_word: true, negation: true}},
                  associated_against: { user: :username }

  belongs_to :user
  validates :target, presence: true, numericality: true
  validates :title, presence: true, uniqueness: true
  validates :description, presence: true, uniqueness: true


  def update_total(amount)
    amount = total + amount unless total.nil?
    self.update_attributes(total: amount)
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      all.order('created_at DESC')
    end
  end


  private

  def set_end_time
    self.end_time = Time.now + 30.days
  end

end
