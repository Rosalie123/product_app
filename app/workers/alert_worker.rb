class AlertWorker
  include Sidekiq::Worker

  def perform(product_id)
    product = Product.find(product_id)
    UserMailer.new_fund_alert(product).deliver_now
  end
end
