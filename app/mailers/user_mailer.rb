class UserMailer < ApplicationMailer

  def new_fund_alert(product)
    @product = product
    mail bcc: User.all.map(&:email), subject: 'Yaofund: New fund published'
  end

end
