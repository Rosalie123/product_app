class ApplicationMailer < ActionMailer::Base
  default from: 'alert@yaofund.com'
  layout 'mailer'
end
