// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on("turbolinks:load", function(){
  $('#donateButton').on('click', function(e) {
    e.preventDefault();

    $('#error_explanation').html('');

    var amount = $('input#amount').val();
    amount = amount.replace(/\$/g, '').replace(/\,/g, '')

    amount = parseFloat(amount);

    if (isNaN(amount)) {
      $('#error_explanation').html("<p class='alert alert-danger'>Please enter a valid amount in USD ($).</p>");
    }
    else if (amount < 5.00) {
      $('#error_explanation').html("<p class='alert alert-danger'>Donation amount must be at least $5.</p>");
    }
    else {
      amount = amount * 100; // Needs to be an integer!
      handler.open({
        amount: Math.round(amount)
      })
    }
  });

  $(window).on('popstate', function() {
    handler.close();
  });
});
