class ChargesController < ApplicationController

  def new
    @product = Product.find(params[:product_id])
  end

  def create
    @product = Product.find(params[:product_id])
    @amount = params[:amount]

    @amount = @amount.gsub('$', '').gsub(',', '')

    begin
      @amount = Float(@amount).round(2)
    rescue
      flash[:danger] = 'Charge not completed. Please enter a valid amount in USD($).'
      redirect_to new_product_charge_path(@product)
      return
    end

    @amount = (@amount * 100).to_i # Must be an integer!

    if @amount < 500
      flash[:danger] = 'Charge not completed. Donation amount must be at least $5.'
      redirect_to new_product_charge_path(@product)
      return
    end

    Stripe::Charge.create(
      :amount => @amount,
      :currency => 'usd',
      :source => params[:stripeToken],
      :description => "Donation to #{@product.title}"
    )
    @product.update_total(@amount/100)

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to new_product_charge_path(@product)
    end

  end
