class ProductsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :authenticate_admin!, except: [:index, :show]

  def index
    @products = Product.text_search(params[:query]).page(params[:page]).per_page(3)
  end

  def new
    @product = Product.new
  end

  def create
    @product = current_user.products.build(product_params)
    if @product.save
      AlertWorker.perform_in(5.minutes, @product.id)
      redirect_to products_path
    else
      render 'new'
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    @product.update_attributes(product_params)
    if @product.save
      redirect_to products_path
    else
      flash[:danger] = 'Something wrong with your form'
      render 'edit'
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:success] = 'Fund deleted successfully'
    redirect_to products_path
  end


  private

  def product_params
    params.require(:product).permit(:description, :title, :target)
  end

  def authenticate_admin!
    redirect_to root_url unless current_user.admin
  end
end
