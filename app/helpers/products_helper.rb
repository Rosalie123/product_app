module ProductsHelper
  def flash_class_name(name)
    case name
    when 'notice' then 'success'
    when 'alert'  then 'danger'
    else name
    end
  end

  def percentage(product)
    product.total.nil? ? 0 : (product.total/product.target)*100
  end
end
